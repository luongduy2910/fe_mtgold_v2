"use server"
import axios from "axios"


//TODO - Kiểm tra recaptcha để lấy token -> nếu hợp lệ -> thực hiện xử lý form 
export const verifyCaptcha = async (token:string) => {
    //NOTE - Fetch url api do gg cung cấp (gửi kèm secret key và token để gg định dạng)
    const res = await axios.post(
        `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET_KEY}&response=${token}`
    )
    if(res.data.score > 0.5){
        //NOTE - Trong trường hợp thành công -> trả boolean true để tiếp tục xử lý form
        return true
    }else {
        //NOTE - trường hợp thất bại -> trả boolen false và ngừng việc xử lý form 
        return false
    }
}