"use server"

import { redirect } from "next/navigation"
import { z } from "zod"

const createOrderSchema = z.object({
    ghiChuThem : z.string().min(5, {message : "Vui lòng ghi chú thêm để chúng tôi có thể phục vụ bạn tốt nhất"})
})

interface CreateOrderFormState {
    errors : {
        ghiChuThem? : string[],
        _form? : string[]
    }
}


export const createOrder = async (nguoiDungId : any, parentName : string , childId : string, formState : CreateOrderFormState, formData : FormData) : Promise<CreateOrderFormState> => {
    const ghiChuThem = formData.get("ghiChuThem");
    const result = createOrderSchema.safeParse({
        ghiChuThem
    });

    if(!result.success){
        return {
            errors : result.error.flatten().fieldErrors
        }
    }

    if(nguoiDungId === null){
        return {
            errors : {
                _form : ["Vui lòng đăng nhập trước khi đặt dịch vụ!!!"]
            }
        }
    }
    
    const dataReq = {
        nguoiDungId,
        dvConId : childId,
        ghiChuThem
    }

    let isSucess = false;

    try {
        const res = await fetch("http://localhost:8788/don-hang", {
            method : "POST", 
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify(dataReq),
        })

        if(res.status === 201){
            isSucess = true;
        }
        
    } catch (error : unknown) {
        if(error instanceof Error){
            return {
                errors : {
                    _form : [error.message]
                }
            }
        }else {
            return {
                errors : {
                    _form : ["Some thing went wrong"]
                }
            }
        }

    }

    if(isSucess) redirect(`/service/${parentName}/${childId}?success=true`);


    return {
        errors : {}
    }
}