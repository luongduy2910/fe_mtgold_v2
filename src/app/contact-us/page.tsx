import { FormContact } from '@/components/FormContact/FormContact'
import ContactGif from '../../../public/img/12121.gif'
import Link from 'next/link'
import ContactBottomImg from '../../../public/img/contact_bottom.png'
import Background from '../../../public/img/background.png'
import Image from 'next/image'
import type { Metadata } from 'next'

export const metadata : Metadata = {
  title : "Liên hệ nhanh | MT Gold",
  description : "Đây là trang liên hệ của MT Gold"
}

interface ContactUsPageProps {
  searchParams: {
    success: string
  }
}

export default function ContactUsPage({ searchParams }: ContactUsPageProps) {
  console.log(searchParams.success)
  if (searchParams.success) {
    return (
      <div className="success__noti flex flex-col justify-center items-center py-40 gap-y-5">
        <svg
          className="w-32 h-32 text-green-500 dark:text-white"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="currentColor"
          viewBox="0 0 20 20"
        >
          <path
            fill="currentColor"
            d="m18.774 8.245-.892-.893a1.5 1.5 0 0 1-.437-1.052V5.036a2.484 2.484 0 0 0-2.48-2.48H13.7a1.5 1.5 0 0 1-1.052-.438l-.893-.892a2.484 2.484 0 0 0-3.51 0l-.893.892a1.5 1.5 0 0 1-1.052.437H5.036a2.484 2.484 0 0 0-2.48 2.481V6.3a1.5 1.5 0 0 1-.438 1.052l-.892.893a2.484 2.484 0 0 0 0 3.51l.892.893a1.5 1.5 0 0 1 .437 1.052v1.264a2.484 2.484 0 0 0 2.481 2.481H6.3a1.5 1.5 0 0 1 1.052.437l.893.892a2.484 2.484 0 0 0 3.51 0l.893-.892a1.5 1.5 0 0 1 1.052-.437h1.264a2.484 2.484 0 0 0 2.481-2.48V13.7a1.5 1.5 0 0 1 .437-1.052l.892-.893a2.484 2.484 0 0 0 0-3.51Z"
          />
          <path
            fill="#fff"
            d="M8 13a1 1 0 0 1-.707-.293l-2-2a1 1 0 1 1 1.414-1.414l1.42 1.42 5.318-3.545a1 1 0 0 1 1.11 1.664l-6 4A1 1 0 0 1 8 13Z"
          />
        </svg>

        <h2 className="text-6xl font-bold">Đã gửi thành công</h2>
        <p>Chúng tôi sẽ liên lạc với bạn sau!</p>
        <Link className='link__services' href={'/'}>Về trang chủ</Link>
      </div>
    )
  } else {
    return (
      <section className="contact w-full">
        <div className="contact__top">
          <h1 className="text-center contact__title font-bold">
            Liên hệ đến MT Gold
          </h1>
          <div className="contact__form p-20 flex flex-row gap-4 items-center">
            <div className='w-1/3'>
              <Image className='w-full h-full' src={ContactGif} alt='gif__contact'/>
            </div>
            <div className="form w-1/2">
              <FormContact />
            </div>
          </div>
        </div>
        <div
          className=" contact__bottom flex flex-row gap-6 justify-center py-10 relative"
        >
          <Image src={Background} alt='background' className='w-full h-full absolute top-0 left-0 -z-10'/>
          <div className="contact__bottom__info w-1/4 border rounded bg-white shadow-lg h-fit p-4 space-y-2">
            <p className="text-lg">Hotline: 0707693260</p>
            <p className="text-lg">
              Email: <span className="underline">mtgold794@gmail.com</span>
            </p>
            <p className="flex flex-row">
              <svg
                className="w-6 h-6 text-gray-800 dark:text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 17 21"
              >
                <g
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                >
                  <path d="M8 12a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z" />
                  <path d="M13.8 12.938h-.01a7 7 0 1 0-11.465.144h-.016l.141.17c.1.128.2.252.3.372L8 20l5.13-6.248c.193-.209.373-.429.54-.66l.13-.154Z" />
                </g>
              </svg>
              XEM BẢN ĐỒ
            </p>
            <p>
              Địa chỉ: 816/64/32/17 Quốc lộ 1, Khu phố 5, Phường Thạnh Xuân,
              Quận 12, Thành phố Hồ Chí Minh, Việt Nam
            </p>
          </div>
          <div className="contact__bottom__img w-1/3">
            <Image src={ContactBottomImg} alt="contact_bottom_img" />
          </div>
        </div>
      </section>
    )
  }
}
