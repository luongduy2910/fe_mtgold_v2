import * as dummyData from "@/utils"
export default async function sitemap() {
    const baseUrl = "https://fe-mtgold-v2.vercel.app";
    const serviceGroupUrls = dummyData.listGroupService.map(serviceGroup => ({
        url : `${baseUrl}/service/${serviceGroup.maNhomDv}`,
        lastModified : new Date(),
    }));
    const serviceUrls : any[] = [];
    dummyData.listGroupService.forEach(serviceGroup => {
        serviceGroup.dichVuCon.forEach(service => {
            const serviceUrl = {
                url : `${baseUrl}/service/${serviceGroup.maNhomDv}/${service.tieuDe}`,
                lastModified : new Date()
            }
            serviceUrls.push(serviceUrl);
        })
    })

    return [
        {
            url : baseUrl,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/about-us`,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/contact-us`,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/profile`,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/admin`,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/auth/register`,
            lastModified : new Date(),
        },
        {
            url : `${baseUrl}/auth/login`,
            lastModified : new Date(),
        },
        ...serviceGroupUrls,
        ...serviceUrls, 
    
    ]
}