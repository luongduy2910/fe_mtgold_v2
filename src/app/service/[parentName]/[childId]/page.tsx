import * as dummyData from '@/utils'
import { IGroupService } from '@/types/interfaces'
import { notFound } from 'next/navigation'
import { Star } from '@/components/Star/Star'
import Image from 'next/image'
import ServiceImg from '../../../../../public/img/servfull.png'
import AdminImg from '../../../../../public/img/adminImg.png'
import Bg from '../../../../../public/img/background.png'
import { FormOrder } from '@/components/FormOrder/FormOrder'
import { FormReview } from '@/components/FormReview/FormReview'

interface DetailServicePageProps {
  params: {
    parentName: string
    childId: string
  }
  searchParams: {
    success : string
  }
}

interface DetailServicePageParams {
  params: {
    parentName: string
    childId: string
  }
}

export function generateMetadata({ params }: DetailServicePageParams) {
  const groupService: IGroupService[] = dummyData.listGroupService.filter(
    (serviceGroup) => serviceGroup.maNhomDv === params.parentName,
  )
  const service = groupService[0]?.dichVuCon?.filter(
    (service) => service.id === params.childId,
  )
  if (!service) {
    return {
      title: 'Không tìm thấy',
      description: 'Không tìm thấy trang bạn cần',
    }
  }
  return {
    title: `${service[0].tieuDe} | Dịch vụ MT Gold`,
    description: 'Dịch vụ MT Gold',
  }
}

//TODO - Lấy nhóm dịch vụ -> pre-render các dịch vụ con trong nhóm đó -> map dữ liệu theo cấu trúc [{childId : 11}, {childId : 12}, ...]
export function generateStaticParams({ params }: DetailServicePageParams) {
  const { parentName } = params
  const groupService = dummyData.listGroupService.filter(
    (groupService) => groupService.maNhomDv === parentName,
  )
  return groupService[0]?.dichVuCon?.map((service) => ({
    childId: service.id,
  }))
}

export default function DetailServicePage({ params, searchParams}: DetailServicePageProps) {
  const { parentName, childId } = params
  const { success } = searchParams;
  console.log(parentName, childId)
  const groupService = dummyData.listGroupService.filter(
    (groupService) => groupService.maNhomDv === parentName,
  )
  const service = groupService[0]?.dichVuCon?.filter(
    (service) => service.id === childId,
  )
  if (!service) {
    return notFound()
  }

  return (
    <section
      className="service__detail py-10 px-60 flex flex-col flex-1 gap-10 relative"
    >
      <Image src={Bg} alt='background' className='-z-10 w-full h-full absolute top-0 left-0'/>
      <div className=" bg-white rounded-large shadow-large container mx-auto p-10 flex flex-row justify-between items-center">
        <div className="service__detail__info space-y-5 w-2/5">
          <h1 className="text-4xl font-bold">{service[0].tieuDe}</h1>
          <div className="info my-3 flex flex-row gap-x-5 text-lg text-gray-500">
            <div className="star pr-5 border-r-2">
              <Star value={service[0].soSao} edit={false} size={15} />
            </div>
            <div className="review pr-5 border-r-2">
              <span>{service[0].danhGia} Đánh giá</span>
            </div>
            <div className="transaction pr-5">
              <span>{service[0].soGiaoDich} Giao dịch</span>
            </div>
          </div>
          <Image
            className="rounded-lg w-full h-full"
            src={ServiceImg}
            alt="serv_img"
          />
          <div className="creator__info flex flex-row items-center gap-3">
            <Image src={AdminImg} alt="admin__img" />
            <div className="info__creator">
              <span className="font-bold text-lg">Admin</span>
              <p className="fullName">Anh Minh</p>
            </div>
          </div>
        </div>
        <div className="form__order w-2/5">
          <FormOrder success={success} parentName={parentName} childId={childId} price={service[0].gia} />
        </div>
      </div>
      <div className="bg-white rounded-large shadow-large container mx-auto p-10">
        <h2 className="text-4xl font-bold">Chi tiết dịch vụ</h2>
        <div className="my-20 text-xl">
          <p>Xin chào quý khách hàng thân mến,</p>
          <p>
            Chúng tôi vô cùng hân hạnh được phục vụ Quý công ty trong quá trình
            thành lập và phát triển kinh doanh tại thị trường Việt Nam. Với gói
            dịch vụ tư vấn thành lập công ty cơ bản của chúng tôi, Quý khách sẽ
            được hỗ trợ với các công đoạn cần thiết như:
          </p>
          <ul className="my-5">
            {service[0].moTaChiTiet.map((moTa, index) => {
              return (
                <li key={index} className="m-1 list-disc font-bold">
                  {moTa.noiDung}
                </li>
              )
            })}
          </ul>
          <p>
            Chúng tôi cam kết mang đến cho Quý khách sự hỗ trợ chất lượng, đồng
            hành trong quá trình khởi nghiệp và phát triển kinh doanh. Hãy để
            chúng tôi góp phần tạo nên sự thành công của công ty của bạn.
          </p>
          <p>Trân trọng,</p>
        </div>
        <h3 className="text-lg">Công ty TNHH dịch vụ tư vấn MT Gold</h3>
      </div>
      <div className='bg-white rounded-large shadow-large container mx-auto p-10'>
        <h2 className="font-bold text-2xl">Đánh giá dịch vụ</h2>
        <FormReview/>
      </div>
    </section>
  )
}
