import * as dummyData from "@/utils"

//TODO - Dựa vào dữ liệu lấy được từ server -> map dữ liệu theo cấu trúc [{parentName : "dich-vu-1"},{parentName : "dich-vu-2"}]
export function generateStaticParams(){
    return dummyData.listGroupService.map(groupService => ({
      parentName : groupService.maNhomDv
    }))
}

interface ServiceLayoutProps {
    children : React.ReactNode
}

export default function ServiceLayout({children} : ServiceLayoutProps) {
    return (
        <>
            {children}
        </>
    )
}


