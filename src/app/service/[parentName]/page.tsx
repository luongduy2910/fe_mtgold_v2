import { ListService } from "@/components/Services/ListService/ListService";
import { IGroupService } from "@/types/interfaces"
import * as dummyData from "@/utils"
import Bg from "../.../../../../../public/img/background.png"
import { notFound } from "next/navigation";
import BannerServ from "../../../../public/img/BgServ.png"
import Image from "next/image";


interface ServiceGroupPageProps {
  params : {
    parentName : string
  }
}

export function generateMetadata({ params } : ServiceGroupPageProps){
  const groupService : IGroupService[] = dummyData.listGroupService.filter(serviceGroup => serviceGroup.maNhomDv === params.parentName);
  if(!groupService){
    return {
      title : 'Không tìm thấy',
      description : 'Không tìm thấy dịch vụ bạn cần',
    }
  }
  return {
    title : `${groupService[0].tenNhomDv} | MT Gold`,
    description : 'Các nhóm dịch vụ tại MT Gold'
  }
}


export default function ServiceGroupPage({ params } : ServiceGroupPageProps) {
  const { parentName } = params;
  const groupService : IGroupService[] = dummyData.listGroupService.filter(serviceGroup => serviceGroup.maNhomDv === parentName);
  if(!groupService){
    return notFound();
  }
  
  return (
    <section className="relative">
        <Image src={Bg} alt='background' className='w-full h-full absolute top-0 left-0 -z-10'/>
        <Image className='w-full' src={BannerServ} alt='banner_serv'/>
        <div className="py-20">
          <ListService groupService={groupService}/>
        </div>
    </section> 

  ) 
}
