import { ListService } from '@/components/Services/ListService/ListService'
import { listGroupService } from '@/utils'
import React from 'react'
import Bg from '../../../public/img/background.png'
import { Metadata } from 'next'
import Image from 'next/image'
import BannerServ from "../../../public/img/BgServ.png"

export const metadata : Metadata = {
  title : 'Các gói dịch vụ | MT Gold - Lựa chọn phù hợp với nhu cầu kế toán của bạn',
  description : 'Đây là trang hiển thị các gói dịch vụ tại MT Gold'
}

export default function ServicePage() {
  return (
    <section
      className="list__service relative"
    >
      <Image className='w-full' src={BannerServ} alt='banner_serv'/>
      <Image src={Bg} alt='background' className='w-full h-full absolute top-0 left-0 -z-10'/>
      <div className='title__service py-10'>
        <h2 className="text-6xl font-bold text-center">Dịch vụ tại MT Gold</h2>
        <div className="underline__custom"></div>
      </div>
      <div className='p-10 w-4/5 mx-auto'>
        <div className="flex flex-col flex-1 gap-10">
          {listGroupService.map((groupService, index) => {
            return <ListService key={index} groupService={[groupService]} />
          })}
        </div>
      </div>
    </section>
  )
}
