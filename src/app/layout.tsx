import { Inter } from 'next/font/google'
import './globals.css'
import NextUIProviderCustom from "@/providers/next-ui-provider"
import { Headers } from '@/components/Headers/Headers'
import { NextAuthProvider } from '@/providers/next-auth-provider'
import Footer from '@/components/Footer/Footer'
import { RecaptchaProvider } from '@/providers/google-recaptcha-provider'
import { Metadata } from 'next'
import { ToastContainer } from 'react-toastify'
import "react-toastify/dist/ReactToastify.css";


const inter = Inter({ subsets: ['latin'] })

/*
export const metadata : Metadata = {
  verification : {
    google : 'google-site-verification=e43YgMO83lIONEh0ZVep9wyGRkjZRm7AbovAA25FGD4'
  }
}
*/


export default function RootLayout({
  children,
  session
}: {
  children: React.ReactNode
  session: any
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <RecaptchaProvider>
          <NextAuthProvider session={session} >
            <NextUIProviderCustom>
                <Headers />
                {children}
                <Footer />
                <ToastContainer/>
            </NextUIProviderCustom>
          </NextAuthProvider>
        </RecaptchaProvider>
      </body>
    </html>
  )
}
