import { Banner } from "@/components/Banner/Banner";
import { IntroductionCompany } from "@/components/Introduction/IntroductionCompany";
import { Statistics } from "@/components/Statistics/Statistics";
import { IntroService } from "@/components/IntroService/IntroService";
import { Question } from "@/components/Question/Question";
import type { Metadata } from "next";


export const metadata: Metadata = {
  title: 'Trang chủ | MT Gold - Chuyên cung cấp dịch vụ kế toán hàng đầu cho doanh nghiệp',
  description: 'Trang chủ MT Gold',
}

export default function HomePage() {
  return (
    <>
      <Banner/>
      <IntroductionCompany/>
      <Statistics/>
      <IntroService/>
      <Question/>
    </>
  )
}
