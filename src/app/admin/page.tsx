import { Metadata } from 'next'

export const metadata : Metadata = {
  robots : {
    index : false,
    nocache : true,
  }
}

function AdminPage() {
  return (
    <div>AdminPage</div>
  )
}

export default AdminPage