import { FormLogin } from '@/components/FormLogin/FormLogin'
import { Metadata } from 'next';
import React from 'react'

export const metadata : Metadata = {
  title : 'Đăng nhập | MT Gold - Truy cập tài khoản kế toán một cách thuận tiện',
  description : 'Đây là trang đăng nhập MT Gold',
}

interface LoginPageProps {
  searchParams : {
    success : string;
    error : string;
    message : string;
    callbackUrl : string;
  },
}

export default function LoginPage({ searchParams } : LoginPageProps) {
  const { success, error, message, callbackUrl } = searchParams;
  console.log(searchParams);
  return (
    <FormLogin error={error} success={success} message={message} callBackUrl={callbackUrl} />
  )
}
