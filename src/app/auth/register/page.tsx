import { FormRegister } from '@/components/FormRegister/FormRegister'
import { Metadata } from 'next'


export const metadata : Metadata = {
  title : 'Đăng ký | MT Gold - Tạo tài khoản để sử dụng dịch vụ kế toán chất lượng',
  description : 'Đây là trang đăng ký MT Gold',
}

export default function RegisterPage() {
  return <FormRegister/>
}
