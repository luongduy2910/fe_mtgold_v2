'use client'
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";

export function RecaptchaProvider({children} : {children : React.ReactNode}) {
  return (
    <GoogleReCaptchaProvider reCaptchaKey='6LeTkHonAAAAAAMSXN6pwYwHa5n9AXveypCvP9OY'>
        {children}
    </GoogleReCaptchaProvider>
  )
}
