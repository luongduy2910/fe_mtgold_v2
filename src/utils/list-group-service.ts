import { IGroupService } from "@/types/interfaces";
import imgServ from "../../public/img/imgserv.png"

export const listGroupService : IGroupService[] = [
    {
        id : 1,
        tenNhomDv : "Dịch vụ thành lập công ty",
        maNhomDv : "dich-vu-thanh-lap-cong-ty",
        dichVuCon : [
            {
                id : "11",
                tieuDe : "Công ty TNHH",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
                
            },
            {
                id : "12",
                tieuDe : "Công ty Cổ phần",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 4,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "13",
                tieuDe : "Chi nhánh công ty, văn phòng đại diện",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 4.7,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "14",
                tieuDe : "Hộ kinh doanh cá thể",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 4.5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
        ]
    },
    {
        id : 2,
        tenNhomDv : "Sửa đổi bổ sung giấy đăng ký kinh doanh",
        maNhomDv : "sua-doi-bo-sung-giay-dang-ky-kinh-doanh",
        dichVuCon : [
            {
                id : "21",
                tieuDe : "Dịch vụ thay đổi tên doanh nghiệp",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "22",
                tieuDe : "Đổi địa chỉ",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "23",
                tieuDe : "Thêm ngành nghề",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "24",
                tieuDe : "Tăng vốn điều lệ",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            }
        ]
    },
    {
        id : 3,
        tenNhomDv : "Dịch vụ khác",
        maNhomDv : "dich-vu-khac",
        dichVuCon : [
            {
                id : "31",
                tieuDe : "Giải thể doanh nghiệp",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "32",
                tieuDe : "Giải thể chi nhánh",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "33",
                tieuDe : "Tạm ngừng kinh doanh",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
        ]
    },
    {
        id : 4,
        tenNhomDv : "Kế toán thuế",
        maNhomDv : "ke-toan-thue",
        dichVuCon : [
            {
                id : "41",
                tieuDe : "Dịch vụ kế toán",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "42",
                tieuDe : "Dịch vụ báo cáo thuế, BCTC",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
            {
                id : "43",
                tieuDe : "Dịch vụ gỡ rối sổ sách kế toán",
                gia : "1.500.000",
                danhGia : 5,
                soGiaoDich : 10,
                hinhAnh : imgServ.src,
                soSao : 5,
                moTaChiTiet : [
                    {
                        id : "1",
                        noiDung : "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
                    },
                    {
                        id : "2",
                        noiDung : "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
                    },
                    {
                        id : "3",
                        noiDung : "Tư vấn cơ bản về thuế và kế toán ban đầu."
                    },
                    {
                        id : "4",
                        noiDung : "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
                    }
                ],
            },
        ]
    }
]