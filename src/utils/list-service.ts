import { IService } from "@/types/interfaces"
import imgServ from "../../public/img/imgserv.png"
export const listService: IService[] = [
    {
        id: "1",
        hinhAnh: imgServ.src,
        tieuDe: "Dịch vụ thành lập công ty",
        gia: "1.500.000 VND",
        danhGia: 5,
        soGiaoDich: 3,
        soSao: 5,
        moTaChiTiet: [
            {
                id: "1",
                noiDung: "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
            },
            {
                id: "2",
                noiDung: "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
            },
            {
                id: "3",
                noiDung: "Tư vấn cơ bản về thuế và kế toán ban đầu."
            },
            {
                id: "4",
                noiDung: "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
            }
        ],
    },
    {
        id: "2",
        hinhAnh: imgServ.src,
        tieuDe: "Chi nhánh công ty, văn phòng đại diện",
        gia: "4.000.000 VND",
        danhGia: 5,
        soGiaoDich: 3,
        soSao: 4,
        moTaChiTiet: [
            {
                id: "1",
                noiDung: "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
            },
            {
                id: "2",
                noiDung: "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
            },
            {
                id: "3",
                noiDung: "Tư vấn cơ bản về thuế và kế toán ban đầu."
            },
            {
                id: "4",
                noiDung: "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
            }
        ],
    },
    {
        id: "3",
        hinhAnh: imgServ.src,
        tieuDe: "Hộ kinh doanh cá thể",
        gia: "1.000.000 VND",
        danhGia: 5,
        soGiaoDich: 3,
        soSao: 5,
        moTaChiTiet: [
            {
                id: "1",
                noiDung: "Tư vấn về loại hình công ty phù hợp và thủ tục thành lập.",
            },
            {
                id: "2",
                noiDung: "Hỗ trợ hoàn thiện hồ sơ, thủ tục đăng ký kinh doanh.",
            },
            {
                id: "3",
                noiDung: "Tư vấn cơ bản về thuế và kế toán ban đầu."
            },
            {
                id: "4",
                noiDung: "Giới thiệu cơ bản về quản lý nguồn lực và thủ tục hành chính."
            }
        ],
    },

]