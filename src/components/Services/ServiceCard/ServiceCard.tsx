import { IService } from "@/types/interfaces"
import { Card, CardBody, CardFooter, Image } from "@nextui-org/react"
import Link from "next/link"
//import Image from "next/image"

interface ServiceCardProps {
    groupServiceCode? : string,
    key : number,
    service : IService,
}

export function ServiceCard({ service, groupServiceCode } : ServiceCardProps) {
  return (
    <Card className="h-[400px]" shadow="sm">
      <Link href={`/service/${groupServiceCode}/${service.id}`}>
        <CardBody className="overflow-visible p-0 w-full">
            <Image isZoomed width={"100%"} src={service.hinhAnh} alt={service.tieuDe} className="w-full h-[250px]"/>
            <div className="p-3 space-y-5">
              <h3 className="text-[16px]">{service.tieuDe}</h3>
              <p className="font-bold">Giá: {service.gia}</p>
            </div>
        </CardBody>
        <CardFooter className="text-small text-gray-500 justify-between">
            <span>Đánh giá: {service.danhGia}</span>
            <span>Số giao dịch: {service.soGiaoDich}</span>
        </CardFooter>
      </Link>
    </Card>   
  )
}
