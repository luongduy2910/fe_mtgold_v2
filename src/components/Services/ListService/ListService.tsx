'use client'
import { IGroupService } from '@/types/interfaces'
import { ServiceCard } from '../ServiceCard/ServiceCard'
import { Navigation, Pagination } from "swiper/modules"
import { Swiper, SwiperSlide } from "swiper/react"
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';


interface ListServiceProps {
    groupService : IGroupService[]
}

export function ListService({ groupService } : ListServiceProps) {
  return (
    <section className="w-[90%] mx-auto p-20 bg-white rounded-2xl shadow-large">
      <div className="list__service__group">
        <h4 className="text-3xl border-b-2 mb-5 border-gray-500 py-2">{groupService[0].tenNhomDv}</h4>
        <Swiper
          modules={[Navigation, Pagination]}
          spaceBetween={50}
          slidesPerView={3}
          onSlideChange={() => console.log("Slide change")}
          autoplay={true}
          navigation
        >
          {groupService[0].dichVuCon.map((service, index) => {
            return (
              <SwiperSlide key={index} className='py-10'>
                <ServiceCard groupServiceCode={groupService[0].maNhomDv} key={index} service={service}/>
              </SwiperSlide>
            ) 
          })}
        </Swiper>
      </div>
    </section>
  )
}

