'use client'

import { useSession } from 'next-auth/react'
import Image from 'next/image'
import userImg from "../../../public/img/userImg.png"
import { Star } from '../Star/Star'
import { Textarea } from '@nextui-org/react'

export function FormReview() {
    const { data: session } = useSession()
    if(!session?.user){
        return (
            <div className='form__review flex flex-row gap-5 mt-10 items-center'>
                <div className='flex flex-col justify-center items-center gap-2 w-[10%]'>
                    <Image src={userImg} alt='user_img'/>
                    <span>Nguoidung1</span>
                </div>
                <form className='w-[90%]'>
                    <div className='flex flex-row items-center justify-between mb-3'>
                        <Star value={5} edit={true} size={30}/>
                        <button className='link__services'>Gửi đánh giá</button>
                    </div>
                    <Textarea height={100} maxRows={10} label="Thêm đánh giá của bạn"/>
                </form>
            </div>
        )
    }
}
