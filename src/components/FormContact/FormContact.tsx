'use client'
import { Button, Input, Textarea } from '@nextui-org/react'
import * as actions from '../../actions'
import { useFormState } from 'react-dom'
import { useGoogleReCaptcha } from 'react-google-recaptcha-v3'
import { verifyCaptcha } from '@/actions/verify-captcha'

export function FormContact() {
    //TODO - Sử dụng hooks recaptcha để tiến hành xác thực 
    const { executeRecaptcha } = useGoogleReCaptcha();
    //NOTE - Bóc action server
    const [formState, action] = useFormState(actions.sendContact, { errors: {} })

    const onSubmit = async (formData : FormData) => {
        if(!executeRecaptcha){
            return;
        }
        //TODO - trong hook executeRecaptcha() cần truyền vào TÊN action trong thẻ form (ở đây là "onSubmit") -> hooks sẽ trả về token nếu hợp lệ
        const token = await executeRecaptcha("onSubmit");
        //TODO - Sau khi lấy được token -> tiến hành xác thực -> giá trị trả về sẽ là boolean
        const verified = await verifyCaptcha(token);
        if(verified){
            //TODO - Nếu true -> bắt đầu tiến hành xử lý form contact 
            action(formData);
        }
    }
    return (
        <form action={onSubmit}>
            <div className="form__row__1 flex flex-row gap-4 items-center mb-2">
                <Input
                    name="fullName"
                    id="fullName"
                    label="Họ Tên"
                    size='lg'
                    isInvalid={!!formState.errors.fullName}
                    errorMessage={formState.errors.fullName?.join(', ')}
                />
                <Input
                    name="phoneNumber"
                    id="phoneNumber"
                    label="Số điện thoại"
                    size='lg'
                    isInvalid={!!formState.errors.phoneNumber}
                    errorMessage={formState.errors.phoneNumber?.join(', ')}
                />
            </div>
            <div className="form__row__2 flex flex-row gap-4 items-center mb-2">
                <Input
                    name="email"
                    id="email"
                    label="Email"
                    size='lg'
                    isInvalid={!!formState.errors.email}
                    errorMessage={formState.errors.email?.join(', ')}
                />
                <Input
                    name="title"
                    id="title"
                    label="Tiêu đề"
                    size='lg'
                    isInvalid={!!formState.errors.title}
                    errorMessage={formState.errors.title?.join(', ')}
                />
            </div>
            <div className="form__row__3 flex flex-row gap-4 mb-2">
                <Textarea
                    label="Nội dung"
                    id="content"
                    name="content"
                    rows={8}
                    disableAutosize
                    isInvalid={!!formState.errors.content}
                    errorMessage={formState.errors.content?.join(', ')}
                />
            </div>
            <div className="flex flex-row justify-end">
                <Button type="submit" className='link__services'>
                    GỬI
                </Button>
            </div>
            {formState.errors._form ? (
                <div className="rounded p-2 bg-red-500 border border-r-red-400 my-2 text-white">
                    {formState.errors._form?.join(', ')}
                </div>
            ) : null}
        </form>
    )
}
