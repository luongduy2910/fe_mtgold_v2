'use client'
import { Accordion, AccordionItem } from "@nextui-org/react";

export function Question() {
  return (
    <section className="p-20 question">
        <h2 className="text-6xl font-bold">Câu hỏi <span className="text-yellow-400">thường gặp</span></h2>
        <div className="list__question mt-10">
            <Accordion variant="shadow">
                <AccordionItem key={'1'} aria-label="Accordion 1" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'2'} aria-label="Accordion 2" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'3'} aria-label="Accordion 3" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'4'} aria-label="Accordion 4" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'5'} aria-label="Accordion 5" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'6'} aria-label="Accordion 6" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'7'} aria-label="Accordion 7" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'8'} aria-label="Accordion 8" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'9'} aria-label="Accordion 9" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'10'} aria-label="Accordion 10" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
                <AccordionItem key={'11'} aria-label="Accordion 11" title="Bạn đang cần tư vấn về loại hình kế toán phù hợp với doanh nghiệp của mình?">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex, numquam? Dolorum deserunt, ipsam eum, tempora totam architecto eveniet error similique ab repellat nam rerum quae natus, laudantium nesciunt quam voluptatum.</p>
                </AccordionItem>
            </Accordion>
        </div>
    </section>
  )
}
