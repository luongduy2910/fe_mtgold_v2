'use client'
import ReactStars from 'react-stars'

interface StarProps {
    value : number;
    edit : boolean;
    size : number;
}

export function Star({ value, edit, size } : StarProps) {
  return (
    <ReactStars value={ value } size={size} count={5} color1="#D9D9D9" color2="#FFC84C" edit={edit} />
  )
}
