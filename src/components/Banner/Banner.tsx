'use client'
import Carousel1 from '../../../public/img/banner_1.png'
import Carousel2 from '../../../public/img/banner_2.png'
import Image from 'next/image'
// import Swiper core and required modules
import { Navigation } from 'swiper/modules'
import { Swiper, SwiperSlide } from 'swiper/react'
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import Link from 'next/link'

export function Banner() {
  return (
    <section className="container home__banner flex flex-row items-center mx-auto justify-between">
      <div className="banner__content w-1/3">
        <p className="welcome__text text-2xl">CHÀO MỪNG BẠN ĐẾN VỚI</p>
        <div className='my-7'>
          <h1 className="company__name leading-[1.2]">
            CÔNG TY TNHH <br /> DỊCH VỤ TƯ VẤN <br /> MT GOLD
          </h1>
        </div>
        <div>
          <Link href={'/service'} className="link__services text-2xl ">KHÁM PHÁ NGAY</Link>
        </div>
      </div>
      <div className="banner__img">
        <Swiper
          // install Swiper modules
          modules={[Navigation]}
          navigation
          className='h-full'
        >
          <SwiperSlide>
            <Image className="w-full" src={Carousel1} alt="carousel_1" />
          </SwiperSlide>
          <SwiperSlide>
            <Image className="w-full" src={Carousel2} alt="carousel_2" />
          </SwiperSlide>
        </Swiper>
      </div>
    </section>
  )
}
