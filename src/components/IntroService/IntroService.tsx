import * as dummyData from "@/utils"
import { ServiceCard } from "../Services/ServiceCard/ServiceCard"
import Link from "next/link"
export function IntroService() {
  return (
    <section className="intro__service px-64 py-20 container mx-auto space-y-10">
        <h2 className="text-6xl font-bold text-center">Dịch vụ tại MT GOLD</h2>
        <p className="text-[32px] text-left">Tại MT GOLD, chúng tôi hiểu rằng mỗi doanh nghiệp có những đặc thù riêng, vì vậy chúng tôi không chỉ cung cấp dịch vụ kế toán tiêu chuẩn mà còn tùy biến phù hợp với nhu cầu cụ thể của từng khách hàng. Đội ngũ chuyên gia kế toán tài năng của chúng tôi sẽ đồng hành và tư vấn cho bạn từ những công việc kế toán hàng ngày đến chiến lược tài chính dài hạn, giúp doanh nghiệp của bạn phát triển bền vững.</p>
        <div className="gap-5 grid grid-cols-3">
          {dummyData.listService.map((service, index) => {
            return (
                <ServiceCard key={index} service = {service}/>
            ) 
          })}
        </div>
        <div className="text-center">
          <Link href={"/service"} className="link__services text-2xl ">KHÁM PHÁ NGAY</Link>
        </div>
    </section>
  )
}
