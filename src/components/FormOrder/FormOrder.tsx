'use client'
import { Checkbox, Textarea } from '@nextui-org/react'
import { useEffect, useState } from 'react'
import { useFormState } from 'react-dom'
import * as actions from '@/actions'
import { toast } from 'react-toastify'
import { useSession } from 'next-auth/react'

interface FormOrderProps {
    price: string
    parentName: string
    childId: string
    success?: string
}

export function FormOrder({
    price,
    parentName,
    childId,
    success,
}: FormOrderProps) {
    const [isSelected, setIsSelected] = useState(false)
    const { data : session } = useSession();
    const [formState, action] = useFormState(
        actions.createOrder.bind(null, session?.user?.id, parentName, childId),
        { errors: {} },
    )
    useEffect(() => {
        if (formState?.errors?._form) {
            toast.error(`${formState?.errors?._form?.join(', ')}`)
        }
    }, [formState])

    return (
        <>
            {success ? (
                <div className="space-y-5 p-10 border-2 border-gray-300 rounded-lg text-center">
                    <div className="flex flex-row justify-center w-full">
                        <svg
                            className="w-32 h-32 text-green-500 dark:text-white"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="currentColor"
                            viewBox="0 0 20 20"
                        >
                            <path
                                fill="currentColor"
                                d="m18.774 8.245-.892-.893a1.5 1.5 0 0 1-.437-1.052V5.036a2.484 2.484 0 0 0-2.48-2.48H13.7a1.5 1.5 0 0 1-1.052-.438l-.893-.892a2.484 2.484 0 0 0-3.51 0l-.893.892a1.5 1.5 0 0 1-1.052.437H5.036a2.484 2.484 0 0 0-2.48 2.481V6.3a1.5 1.5 0 0 1-.438 1.052l-.892.893a2.484 2.484 0 0 0 0 3.51l.892.893a1.5 1.5 0 0 1 .437 1.052v1.264a2.484 2.484 0 0 0 2.481 2.481H6.3a1.5 1.5 0 0 1 1.052.437l.893.892a2.484 2.484 0 0 0 3.51 0l.893-.892a1.5 1.5 0 0 1 1.052-.437h1.264a2.484 2.484 0 0 0 2.481-2.48V13.7a1.5 1.5 0 0 1 .437-1.052l.892-.893a2.484 2.484 0 0 0 0-3.51Z"
                            />
                            <path
                                fill="#fff"
                                d="M8 13a1 1 0 0 1-.707-.293l-2-2a1 1 0 1 1 1.414-1.414l1.42 1.42 5.318-3.545a1 1 0 0 1 1.11 1.664l-6 4A1 1 0 0 1 8 13Z"
                            />
                        </svg>
                    </div>
                    <h2 className="text-2xl font-bold">Đặt đơn hàng thành công</h2>
                    <p className="text-lg text-gray-400">
                        Cảm ơn quý khách đã đặt dịch vụ tại MT GOLD.
                        <br />
                        Chúng tôi sẽ liên hệ sớm nhất có thể.
                    </p>
                </div>
            ) : (
                <form action={action}>
                    <div className="form__order space-y-5 p-10 border-2 border-gray-300 rounded-lg">
                        <h2 className="text-2xl font-bold text-center">Giá : ~ {price}</h2>
                        <Textarea
                            isInvalid={!!formState?.errors?.ghiChuThem}
                            errorMessage={formState?.errors?.ghiChuThem?.join(', ')}
                            name="ghiChuThem"
                            id="ghiChuThem"
                            label="Ghi chú"
                        />
                        <div>
                            <Checkbox
                                onValueChange={() => setIsSelected(!isSelected)}
                                isSelected={isSelected}
                                isRequired={true}
                            >
                                Xác nhận đặt đơn
                            </Checkbox>
                        </div>
                        <button
                            type="submit"
                            className={`block w-full rounded-3xl border border-black p-3 bg-white ${isSelected
                                    ? 'hover:bg-black hover:text-white hover:border-white transition-all cursor-pointer'
                                    : 'disabled:opacity-30 cursor-not-allowed'
                                }`}
                            disabled={!isSelected}
                        >
                            Đặt dịch vụ
                        </button>
                    </div>
                </form>
            )}
        </>
    )
}
