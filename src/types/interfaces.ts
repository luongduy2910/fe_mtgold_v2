export interface User {
    name? : string | undefined | null;
    role? : string;
    userName? : string;
    accessToken? : string,
    refreshToken? : string,
    id? : number
}

export interface Posts {
    id : number,
    content : string,
    description : string,
    title : string,
    categoryId : number,
}

export interface IDescription {
    id : string,
    noiDung : string,
}

export interface IService {
    id : string,
    hinhAnh : string,
    tieuDe : string,
    gia : string,
    danhGia : number,
    soGiaoDich : number
    soSao : number,
    moTaChiTiet : IDescription[],

}

export interface IGroupService {
    id : number,
    tenNhomDv : string,
    maNhomDv : string,
    dichVuCon : IService[],
}